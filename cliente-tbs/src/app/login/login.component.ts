import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AuthenticationService} from "../_services";
import { MatDialog } from "@angular/material";
import {LoginAlertComponent} from "../_services/alerts/login-alert/login-alert.component";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthenticationService,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {

    if(this.auth.status() === true){
      this.router.navigate(['/home'])
    }


    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    var user = this.loginForm.value
    this.auth.login(user.email, user.password).subscribe(users => {
      user = users


      this.router.navigate(['/home/dashboard'])
    }, (err) => {
      this.openDialog()

    })
  }

  openDialog() {
    this.dialog.open(LoginAlertComponent, {
      data: {
        animal: 'panda'
      }
    });
  }

}



