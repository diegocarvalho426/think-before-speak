import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../_services";
import {map} from "rxjs/operators";
import {AdvertisementsInterface} from "../../../_interfaces/advertisementsInterface";
import {Observable} from "rxjs";
import {MatDialog} from "@angular/material";
import {ResultsComponent} from "../results/results.component";


@Component({
  selector: 'app-sharing-area',
  templateUrl: './sharing-area.component.html',
  styleUrls: ['./sharing-area.component.css']
})
export class SharingAreaComponent implements OnInit {

  advertisements
  constructor(
    private us: UserService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.us.getSharingArea().subscribe(response=> {
      this.advertisements = response[0]['advertisements']
      console.log(response[0]['advertisements'])
    })
  }



  openDialog() {
    const dialogRef = this.dialog.open(ResultsComponent).updateSize('50%');
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }


  delete(){
    console.log("click")
  }

}
