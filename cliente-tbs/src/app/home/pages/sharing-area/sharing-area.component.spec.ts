import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharingAreaComponent } from './sharing-area.component';

describe('SharingAreaComponent', () => {
  let component: SharingAreaComponent;
  let fixture: ComponentFixture<SharingAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharingAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharingAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
