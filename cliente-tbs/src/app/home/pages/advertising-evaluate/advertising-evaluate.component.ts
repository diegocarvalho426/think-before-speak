import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from "../../../_services";
import {MatDialog} from "@angular/material";
import {ResultsComponent} from "../results/results.component";
import {Router} from "@angular/router";
import {EvaluateAlertComponent} from "../../../_services/alerts/evaluate-alert/evaluate-alert.component";

@Component({
  selector: 'app-advertising-evaluate',
  templateUrl: './advertising-evaluate.component.html',
  styleUrls: ['./advertising-evaluate.component.css']
})
export class AdvertisingEvaluateComponent implements OnInit {

evaluateForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private us: UserService,
    private dialog: MatDialog,
    private rs: Router,
  ) { }

  ngOnInit() {
    this.evaluateForm = this.fb.group({
      advertising: ['', Validators.required]
    })
  }

  onSubmit(){
    let advertisement = this.evaluateForm.value
    this.us.evaluate(advertisement).subscribe((response: Response) =>{
      this.rs.navigate(['home/results'])
    }, (error)=> {
      console.log(error)
    })
  }

  openDialog() {
    const dialogRef = this.dialog.open(EvaluateAlertComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }


}
