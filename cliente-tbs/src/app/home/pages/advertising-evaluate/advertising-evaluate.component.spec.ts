import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertisingEvaluateComponent } from './advertising-evaluate.component';

describe('AdvertisingEvaluateComponent', () => {
  let component: AdvertisingEvaluateComponent;
  let fixture: ComponentFixture<AdvertisingEvaluateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertisingEvaluateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertisingEvaluateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
