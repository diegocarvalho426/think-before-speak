import { Component, OnInit } from '@angular/core';
import {AuthenticationService, UserService} from "../../../_services";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Route, Router} from "@angular/router";
import {UpdateUserAlertComponent} from "../../../_services/alerts/update-user-alert/update-user-alert.component";
import { MatDialog } from "@angular/material";


@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
  user
  updateForm: FormGroup

  constructor(
    private us: UserService,
    private fb: FormBuilder,
    private router: Router,
    private auth: AuthenticationService,
    private dialog: MatDialog,
  ) {
  }

  ngOnInit() {


    this.us.getUserInfo().subscribe(response => {
      console.log('service response')
      console.log(response.newUser)
      this.user = response.newUser
      console.log('response component')
      console.log(this.user)
    })

    this.updateForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
    });


  }

  cancelOption() {
    this.router.navigate(
      ["/login"]
    )
  }

  onSubmit() {

    let newUser = this.updateForm.value
    this.us.update(newUser).subscribe(response => {
      this.auth.logout()
      this.openDialog()
    }, (e) => {
      console.log(e)
    })
  }

  openDialog() {
    this.dialog.open(UpdateUserAlertComponent).updateSize("50%");
  }
}
