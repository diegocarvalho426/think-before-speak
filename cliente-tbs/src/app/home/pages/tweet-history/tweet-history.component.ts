import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../_services";
import {map} from "rxjs/operators";
import {AdvertisementsInterface} from "../../../_interfaces/advertisementsInterface";
import {Observable} from "rxjs";
import {MatDialog} from "@angular/material";
import {ResultsComponent} from "../results/results.component";


@Component({
  selector: 'app-tweet-history',
  templateUrl: './tweet-history.component.html',
  styleUrls: ['./tweet-history.component.css']
})
export class TweetHistoryComponent implements OnInit {

  private advertisements: AdvertisementsInterface[]
  constructor(
    private us: UserService,
    private dialog: MatDialog
    ) { }

  ngOnInit() {
     this.us.getUserAdvertisements().subscribe(response=> this.advertisements = response)

  }



  openDialog() {
    const dialogRef = this.dialog.open(ResultsComponent).updateSize('50%');
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }


  delete(){
    console.log("click")
  }

}
