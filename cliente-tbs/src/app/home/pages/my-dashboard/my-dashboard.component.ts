import { Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from "../../../_services";
import {Chart} from "Chartjs"
import {ChangeDetectorRef} from '@angular/core';



@Component({
  selector: 'app-my-dashboard',
  templateUrl: './my-dashboard.component.html',
  styleUrls: ['./my-dashboard.component.css']
})
export class MyDashboardComponent implements OnInit{
  formGroup: FormGroup;
  options = []
  show: boolean
  //lineChartConfig

  constructor(
    private formBuilder: FormBuilder,
    private us: UserService,
    private change: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.show=false
    this.fillCompanyNamesOptions()
    this.formGroup = this.formBuilder.group({
      company1: ['', Validators.required],
      company2: ['', Validators.required]
      }
    )
  }
  /** Based on the screen size, switch from standard to one column per row */

  fillCompanyNamesOptions() {
    this.us.getCompanyNamesList().subscribe(
      response=>
        this.options = response.companyNames
    )

  }

  analyzeCompanies() {
    console.log(this.formGroup.value)
    this.us.getCompaniesTweets(this.formGroup.value['company1'], this.formGroup.value['company2']).subscribe(data=>{
      console.log(data)
    })
  }



}
