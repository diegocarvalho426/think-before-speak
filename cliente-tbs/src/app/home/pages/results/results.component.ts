import {Component, Inject, OnInit} from '@angular/core';
import {Chart} from 'chart.js'
import {Tile} from "../../../_interfaces/tile";
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})



export class ResultsComponent implements OnInit {

  //config
  dialog

  options = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }

  barChart = []
  pieChart
  constructor() { }

  ngOnInit() {
    this.generateCharts()
  }



  generateCharts(){
    Chart.defaults.global.animationSteps = Math.round(5000 / 17);
    //generate bar chart
    this.barChart = new Chart('chartOne', {
      type: 'bar',
      data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
          label: '# of Votes',
          data: [12, 19, 3, 5, 2, 3],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: this.options,
    });

    //generate pie chart

    this.pieChart = new Chart('pieChart',{
      type: 'doughnut',
      data: {
        labels: [
          "O quanto gostaram",
        ],
        datasets: [{
          data: [80, 20],
          backgroundColor: [
            'rgba(128, 255, 128, 1)',
            'rgba(203, 203, 179, 1)'
          ],
          borderColor: [
            'rgba(128, 255, 128, 1)',
            'rgba(203, 203, 179, 1)'
          ]
        }],

      },
      options: this.options,
    });

  }



}
