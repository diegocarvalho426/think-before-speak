import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {AuthenticationService, UserService} from "../../../_services"
import {User} from "../../../_models";
import {Router} from "@angular/router";

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  private loggedUser: User

  constructor(
    private breakpointObserver: BreakpointObserver,
    private at: AuthenticationService,
    private us: UserService,
    private route: Router
  ) {
  }

  ngOnInit() {
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
  }

  logout() {
    this.at.logout()
  }

  updateUser(){
    this.route.navigate(['../update'])
  }

  refreshAPI(){
    window.location.reload()
  }

}
