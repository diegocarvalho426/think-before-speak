import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule} from '@angular/forms';
import { routing } from './app.routing';
import { MatFormFieldModule, MatInputModule, MatCardModule, MatStepperModule, MatButtonModule,MatSelectModule, MatOptionModule, MatIconModule, MatToolbarModule, MatSidenavModule, MatListModule, MatGridListModule, MatMenuModule, MatDivider, MatDialogContent, MatDialogClose, MatDialogActions, MatDialogModule } from '@angular/material';
import { MyNavComponent } from './home/components/my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { HomeComponent } from './home/home.component';
import { MyDashboardComponent } from './home/pages/my-dashboard/my-dashboard.component';
import { AdvertisingEvaluateComponent } from './home/pages/advertising-evaluate/advertising-evaluate.component';
import { TweetHistoryComponent } from './home/pages/tweet-history/tweet-history.component';
import { UserService } from "./_services/user.service"
import {AuthenticationService} from "./_services";
import { LoginAlertComponent } from './_services/alerts/login-alert/login-alert.component';
import { ResultsComponent } from './home/pages/results/results.component';
import { EvaluateAlertComponent } from './_services/alerts/evaluate-alert/evaluate-alert.component';
import { ResultsHistoryAlertComponent } from './_services/alerts/results-history-alert/results-history-alert.component';
import { UpdateUserComponent } from './home/pages/update-user/update-user.component';
import { UpdateUserAlertComponent } from './_services/alerts/update-user-alert/update-user-alert.component';
import { SharingAreaComponent } from './home/pages/sharing-area/sharing-area.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    MyNavComponent,
    HomeComponent,
    MyDashboardComponent,
    AdvertisingEvaluateComponent,
    TweetHistoryComponent,
    LoginAlertComponent,
    ResultsComponent,
    EvaluateAlertComponent,
    ResultsHistoryAlertComponent,
    UpdateUserComponent,
    UpdateUserAlertComponent,
    SharingAreaComponent,
  ],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatGridListModule,
    MatMenuModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatStepperModule
  ],
  entryComponents: [
    LoginAlertComponent,
    ResultsComponent,
    UpdateUserAlertComponent

  ],
  providers: [
    UserService,
    AuthenticationService,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
