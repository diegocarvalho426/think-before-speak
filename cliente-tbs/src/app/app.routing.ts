import { Routes, RouterModule } from '@angular/router';

//import { HomeComponent } from './home';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { MyDashboardComponent } from './home/pages/my-dashboard/my-dashboard.component';
import { AdvertisingEvaluateComponent } from './home/pages/advertising-evaluate/advertising-evaluate.component'
import {TweetHistoryComponent} from "./home/pages/tweet-history/tweet-history.component";
import {ResultsComponent} from "./home/pages/results/results.component";
import {UpdateUserComponent} from "./home/pages/update-user/update-user.component";
import {LoginAuthGuardService} from "./_services/login-auth-guard/login-auth-guard.service";
import {SharingAreaComponent} from "./home/pages/sharing-area/sharing-area.component";

const appRoutes: Routes = [
  //{ path: '', component: HomeComponent, canActivate:},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent, canActivate:[LoginAuthGuardService],
    children: [
      {path:'evaluate', component:AdvertisingEvaluateComponent},
      {path:'history', component: TweetHistoryComponent},
      {path:'dashboard',component:MyDashboardComponent},
      {path: 'results', component: ResultsComponent},
      {path: 'company', component: SharingAreaComponent}
    ]
    },
  { path: 'update', component: UpdateUserComponent},

  // otherwise redirect to home
  { path: '',   redirectTo: 'login', pathMatch: 'full' },
  { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
