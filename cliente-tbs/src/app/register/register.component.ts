import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { RegisterService} from './register.service';
import { Routes, RouterModule, Router } from '@angular/router';
import {MatFormFieldControl} from '@angular/material';
import {map} from "rxjs/operators";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers:[ RegisterService ],
})
export class RegisterComponent implements OnInit {
  userForm: FormGroup;
  public companyOptions = []
  show

  constructor(
    private fb: FormBuilder,
    private rs:   RegisterService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      repeatPassword: ['', [Validators.required, Validators.minLength(6)]],
      companyName: ['',Validators.required]
    });

    this.fillCompanyNamesOptions()
    console.log(this.companyOptions)
  }

  onSubmit() {
    console.log('click')
    const user = this.userForm.value;
    this.rs.addUser(user).subscribe(
      response => {
        this.router.navigate(['/login'])
      },
      err => console.log(err),
    );
  }

  fillCompanyNamesOptions() {
    this.rs.getCompanyNamesList().subscribe(
      response=>
        this.companyOptions=response.companyNames
    )
  }

  cancelOption(){
    this.router.navigate(['/login'])
  }


}
