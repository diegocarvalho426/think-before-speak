import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User } from '../_models';
import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private  http: HttpClient) {}

  addUser(user: any): Observable<User> {
    const options = new HttpHeaders()
      .set('Authorization', 'my-auth-token')
      .set('Content-Type', 'application/json');

    console.log(`${environment.apiUrl}`.concat('api/v1/users'));
    console.log(JSON.stringify(user));
    return this.http.post<User>(`${environment.apiUrl}`.concat('users'), JSON.stringify(user), {
      headers: options
    });

  }

  getCompanyNamesList() : Observable<any>{
    return this.http.get(`${environment.apiUrl}`.concat('company/names'))
  }
}
