import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluateAlertComponent } from './evaluate-alert.component';

describe('EvaluateAlertComponent', () => {
  let component: EvaluateAlertComponent;
  let fixture: ComponentFixture<EvaluateAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluateAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluateAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
