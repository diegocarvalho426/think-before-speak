import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsHistoryAlertComponent } from './results-history-alert.component';

describe('ResultsHistoryAlertComponent', () => {
  let component: ResultsHistoryAlertComponent;
  let fixture: ComponentFixture<ResultsHistoryAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsHistoryAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsHistoryAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
