import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUserAlertComponent } from './update-user-alert.component';

describe('UpdateUserAlertComponent', () => {
  let component: UpdateUserAlertComponent;
  let fixture: ComponentFixture<UpdateUserAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateUserAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUserAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
