﻿import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Routes, RouterModule, Router } from "@angular/router";
import { User } from '../_models'

import { environment } from '../../environments/environment';
import {Observable} from "rxjs";

@Injectable()
export class AuthenticationService {


  constructor(
    private http: HttpClient,
    private rs: Router
  ) { }


  login(email: string, password: string): Observable<any> {
    console.log('click2')
    return this.http.post<User>(`${environment.apiUrl}`.concat('users/login'), { email: email, password: password })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.tokens) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('user', JSON.stringify(user));
        }

        return user;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    let user = JSON.parse(localStorage.getItem('user'))
    console.log(user.tokens[user.tokens.length - 1].token)
    const options = new HttpHeaders()
      .set('x-auth', user.tokens[user.tokens.length - 1].token)
      .set('Content-Type', 'application/json')


    this.http.delete(`${environment.apiUrl}`.concat('users/logout'), {headers: options}).subscribe(result => {
      localStorage.removeItem('user')
      this.rs.navigate(['/login'])
    }, (e) => {
      console.log(e)
    })
  }


   status (): Boolean {
    let user = localStorage.getItem('user')
    if(user === null){
      return false
    } else {
      return true
    }

  }

}
