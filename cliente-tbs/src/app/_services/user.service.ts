﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User } from '../_models';
import {UserInterface} from "../_interfaces/userInterface";
import {Observable} from "rxjs";
import { AdvertisementsInterface } from "../_interfaces/advertisementsInterface";
@Injectable()
export class UserService {



  constructor(private http: HttpClient) { }


  getUserAdvertisements() : Observable<AdvertisementsInterface[]> {
    return this.http.get<AdvertisementsInterface[]>(`${environment.apiUrl}`.concat('/me/advertisements'), {headers: this.getUserTokenAcess()})
  }

  getUserInfo(): Observable<any> {
    return this.http.get(`${environment.apiUrl}`.concat('/users/me'), {headers: this.getUserTokenAcess()})
  }

  evaluate(advertisement): Observable<any> {
    return this.http.post(`${environment.apiUrl}`.concat('/me/advertisements'), advertisement, {headers:this.getUserTokenAcess()})
  }

  getUserTokenAcess(): HttpHeaders{
    let user = JSON.parse(localStorage.getItem('user'))
    const options = new HttpHeaders()
      .set('x-auth', user.tokens[user.tokens.length - 1].token)
      .set('Content-Type', 'application/json');

    console.log(options)
    return options
  }

  getById(id: number) {
    return this.http.get(`${environment.apiUrl}/users/` + id);
  }

  register(user: User) {
    return this.http.post(`${environment.apiUrl}/users/register`, user);
  }

  update(user: User) {
    return this.http.put(`${environment.apiUrl}/users/me`, user,{headers: this.getUserTokenAcess()});
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}/users/` + id);
  }

  getCompanyNamesList() : Observable<any>{
    return this.http.get(`${environment.apiUrl}`.concat('company/names'))
  }

  getCompaniesTweets(company1: string, company2: string): Observable<any>{
    return this.http.get(`${environment.apiUrl}`.concat('/me/companies/?company1='+ company1 + '&company2=' + company2), {headers: this.getUserTokenAcess()})
  }

  getSharingArea(){
    return this.http.get(`${environment.apiUrl}`.concat('/me/shared'), {headers: this.getUserTokenAcess()})
  }



}
