import {AdvertisementsInterface} from "./advertisementsInterface";

export interface UserInterface {
  id: number;
  email: string;
  name: string;
  password: string;
  tokens: Array<String>;
  companyName: String;
  advertisements: AdvertisementsInterface[]
}
