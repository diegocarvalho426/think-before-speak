﻿import {Advertisements} from "./advertisements";

export class User {
  id: number;
  email: string;
  name: string;
  password: string;
  tokens: Array<String>;
  companyName: String;
  advertisements: Advertisements[]
}
