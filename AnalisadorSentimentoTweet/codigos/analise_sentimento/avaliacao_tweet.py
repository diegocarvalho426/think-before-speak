#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pickle


class AvaliacaoTweet:
    """
    Descrição
    ---------
    Esta classe tem como objeto avaliar o sentimento relacionado à um tweet. Para isso,
    utiliza-se de um modelo de classificação treinado previmente para realizar a pre-
    dição do sentimento. Também é necessário a utilização do objeto TfIdfVectorizer que
    foi utilizado para treinar o modelo, sendo este necessário para obter o vetor TF-IDF
    do tweet a ser avaliado.

    Parâmetros
    ----------
    :param arq_modelo: str
        Caminho para o modelo pré treinado salvo com pickle.
    :param arq_tfidf_matriz: str
        Caminho para o objeto TfIdfVectorizer salvo com pickle.

    Atributos
    ---------
    arq_modelo: str
        Caminho para o modelo pré treinado.
    arq_tfidf_matriz: str
        Caminho para o objeto TfIdfVectorizer.
    modelo: object (e.g., MultinomialNB, BernoulliNB, etc)
        Modelo de classificação.
    vect: TfIdfVectorizer
        Objeto TfIdfVectorizer para geração do vetor do tweet.
    """

    def __init__(self, arq_modelo, arq_tfidf_matriz):
        self.arq_modelo = arq_modelo
        self.arq_tfidf_matriz = arq_tfidf_matriz

        self.modelo = self.__carregar_modelo()
        self.vect = self.__carregar_tfidf_matriz()

    def __carregar_modelo(self):
        """
        Descrição
        ---------
        Faz o carregando do modelo scikit-learn treinado previamente e salvo
        em um arquivo com pickle.

        Retorno
        -------
        :return modelo:
            Modelo scikit-learn pré treinado.
            Exemplos: MultinomialNB, BernoulliNB, etc...
        """
        modelo = pickle.load(open(self.arq_modelo, 'rb'))

        return modelo

    def __carregar_tfidf_matriz(self):
        """
        Descrição
        ---------
        Faz o carregando do modelo TfIdfVectorizer utilizado para treinamento
        do modelo.

        Retorno
        -------
        :return modelo: TfIdfVectorizer.
        """
        vect = pickle.load(open(self.arq_tfidf_matriz, 'rb'))

        return vect

    def analise_sentimento_tweet(self, tweet):
        """
        Descrição
        ---------
        Avalia o sentimento de um tweet de acordo com o modelo de classificação
        pré treinado e o objeto TfIdfVectrizer usado para treiná-lo.

        Parâmetros
        ----------
        :param tweet: str
            Tweet a ser avaliado.

        Retorno
        -------
        :return: str
            Sentimento associado ao tweet de acordo com a predição realizada pelo modelo.
        """
        tweet_vect = self.vect.transform([str(tweet)])
        sentimento = self.modelo.predict(tweet_vect)

        if sentimento[0] == -1:
            return -1
        elif sentimento[0] == 1:
            return 1
        else:
            return 0
