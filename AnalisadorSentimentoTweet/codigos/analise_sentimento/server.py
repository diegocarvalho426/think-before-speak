#!/usr/bin/env python3

# Para executar
# $ export FLASK_APP=server.py
# $ python3 -m flask run

from flask import Flask
from flask import request
from pymongo import MongoClient

import json
from avaliacao_tweet import AvaliacaoTweet

client = MongoClient()
db = client['tbs']

arq_modelo = """../../exemplo_analise_sentimento_ptbr/outputs/modelo_multinomialnb.sav"""
arq_tfidf_vect = """../../exemplo_analise_sentimento_ptbr/outputs/tfidf_vec_claro.sav"""

app = Flask(__name__)

aval = AvaliacaoTweet(arq_modelo=arq_modelo, arq_tfidf_matriz=arq_tfidf_vect)
resp = {}

@app.route('/predict', methods=['POST'])
def listen():
    print(request.is_json)
    content = request.get_json()
    resp['tweet'] = content['tweet']
    resp['value'] = aval.analise_sentimento_tweet(content['tweet'])
    return json.dumps(resp)
    
@app.route('/empresas', methods=['GET'])
def sendData():
	data = []
	for col in db.collection_names():
		data.append(list(db[col].aggregate([ { "$group": {"_id": {"nome": col.split("_")[1], "mes": {"$month": {"$dateFromString": {"dateString": "$data"}}}, "ano": {"$year": {"$dateFromString": {"dateString": "$data"}}}}, "positivo": {"$sum": {"$cond": [{"$gt": ['$sentimento_categoria', 0]}, '$sentimento_categoria', 0]}},"negativo": {"$sum": {"$cond": [{"$lt": ['$sentimento_categoria', 0]}, 1, 0]}},"neutro": {"$sum": {"$cond": [{"$eq": ['$sentimento_categoria', 0]}, 1, 0]}}}} ])))
	print(data)
	return json.dumps(data)
	
