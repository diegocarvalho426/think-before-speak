from analise_sentimento.avaliacao_tweet import AvaliacaoTweet

if __name__ == '__main__':
    tweet = "Bom dia. Hoje o dia está lindo!"
    arq_modelo = """../../exemplo_analise_sentimento_ptbr/outputs/modelo_multinomialnb.sav"""
    arq_tfidf_vect = """../../exemplo_analise_sentimento_ptbr/outputs/tfidf_vec_claro.sav"""

    avaliador = AvaliacaoTweet(arq_modelo=arq_modelo,
                               arq_tfidf_matriz=arq_tfidf_vect)

    print(tweet)
    print(avaliador.analise_sentimento_tweet(tweet))
