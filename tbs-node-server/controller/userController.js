
var {User} = require('../models/user');
var {Company} = require('../models/company')
var {Advertisiments} = require('../models/advertisiments')


exports.getUsersList = function(req, res) {
    User.find().then((userList) => {
        res.send({userList})
    }, (e) => {
        res.status(400).send(e)
    })
}

exports.createUser = function(req, res) {
    Company.findOne({name: req.body.companyName}).then((company) => {
        if (company !== null){
            var user = new User({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password,
                companyName: req.body.companyName
            })

            user.save().then((data) => {
                try {
                    company.members = company.members.concat({email: req.body.email})
                }catch (e) {
                    company.members = []
                    company.members.push({email: req.body.email})
                }
                company.save().then((data)=>{
                },(e) =>{
                    console.log(e)
                })
                return user.generateAuthToken()
            }).then((token) => {
                res.header('x-auth', token).send(user)
            }).catch((e) => {
                console.log(e)
                res.status(400).send(e)
            })
        } else {
            res.status(400).send('invalid Company')
        }


    }, (e)=> {
        res.status(400).send('invalid Company')
    })
}

exports.userLogin = function(req, res) {
    var login = req.body.email
    var password = req.body.password

    User.findByCredentials(login, password).then((user) => {
        user.generateAuthToken().then((token) => {
            newUser = {
                name: user.name,
                email:user.email,
                tokens: user.tokens,
            }
            res.header('x-auth', token).send(newUser)
        })
    }).catch((e) => {
        res.sendStatus(400)
    })
}

exports.userLogout = function(req, res) {
    User.findByToken(req.token).then((user) => {
        user.tokens = []
        user.save()
        res.status(200).send()

    }, (e) => {
        res.sendStatus(400)
    })
}

exports.userDelete = function(req, res) {
    var id
    User.findByToken(req.token).then((user)=> {
        User.findById({_id: user._id}).then((user)=> {
            Company.findOne({name: user.companyName}).then((company)=> {
                for(var i = 0; i< company.members.length; i++){
                    if(company.members[i].email === user.email){
                        company.members.pop(i)
                    }
                }
                company.save()
            }, (e)=>{
                console.log(e)
            })
        },(e)=> {
            console.log(e)
        })
    },(e)=> {
        console.log(e)
    })
    User.findByIdAndRemove({_id: req.user._id}).then((user)=> {
        res.sendStatus(200)
    }, (e) => {
        res.sendStatus(404)
    })


}

exports.evaluateAdvertising = function(req, res) {
    let advertisingText = req.body.advertising
    User.findByToken(req.token).then((user) => {
        let advertisiment = new Advertisiments({
            text: advertisingText,
            date: getDateTime(),
            companyName: user.companyName,
            email: user.email,
            results: ''
        })


        var options = {
            url: 'http://localhost:5000/predict',
            method: 'POST',
            json: {
                tweet: req.body.advertising
            }
        }
        request = require('request')
        request(options,(err,response,body)=> {
            if(err) res.sendStatus(400)

            if(body){
                advertisiment.results = body['value']
                console.log(advertisiment)
                saveAdvertisiment(advertisiment, user, req, res)
            }
        })

    })
}


exports.getMe = function(req, res) {
    User.findById({_id: req.user._id}).then((user)=> {
        let newUser = new User({
            name: user.name,
            email: user.email,
            advertisements: user.advertisements,
        })
        console.log(newUser)
        res.status(200).send({newUser})
    }, (e)=> {
        console.log(e)
        res.status(400).send(e)
    })
}

exports.getCompanyTweets = function(req, res){
    console.log(req.query.company1)
    var options = {
        url: 'http://localhost:5000/companies/params?company1=' + req.query.company1 +'&company2=' + req.query.company2,
        method: 'GET',
    }
    request = require('request')
    request(options,(err,response,body)=> {
        if(err) res.status(400).send(err)

        if(body){
            res.status(200).send(body)
        }
    })
}

exports.deleteAdvertisement = function(req, res) {
    console.log(req.user._id)
    User.find({"_id": req.user._id}).then((user)=>{
        console.log(req.body.index)
        user.advertisements.splice(req.body.index,1)
        user.save()
         res.sendStatus(200)
    },(e)=>{
        console.log(e)
        res.status(400).send("deu merda")
    })
}

exports.userUpdate = function (req,res) {
    User.findByToken(req.headers['x-auth']).then((target) => {
        User.findOneAndUpdate({_id: target._id}, req.body).then((user) => {
            res.status(200).send({user})
        }, (e) => {
            console.log('not found')
            res.sendStatus(404)
        })
    },(e) => {
        console.log('not found')
        res.sendStatus(404)
    })
}

exports.getAdvertisements = function (req, res) {
    User.findById({_id: req.user._id}).then((user)=> {
        console.log(JSON.stringify(user.advertisements))
        res.status(200).send(JSON.stringify(user.advertisements))
    }, (e)=> {
        console.log(e)
        res.status(400).send(e)
    })
}

exports.getSharingAre = function (req, res) {
    Company.find({name: req.user.companyName}).then((company)=>{
        res.status(200).send(JSON.stringify(company))
    },(e)=> {
        res.status(400).send(e)
    })

}


function getDateTime() {
    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;
}


function saveAdvertisiment(advertisiment, user, req, res){
    advertisiment.save().then((data)=> {
        Company.findOne({name: user.companyName}).then((company)=> {
            if(company !== null){
                try{
                    company.advertisements.push({
                        text: advertisiment.text,
                        date: advertisiment.date,
                        createdBy: advertisiment.email,
                    })
                }catch (e) {
                    company.advertisements = []
                    company.advertisements.push({
                        text: advertisiment.text,
                        date: advertisiment.date,
                        createdBy: advertisiment.email,
                    })
                }
                company.save().then((data)=> {
                    User.findById(user.id).then((user)=> {
                        user.advertisements.push({
                            text: advertisiment.text,
                            date: advertisiment.date,
                            results: advertisiment.results
                        })
                        user.save().then((data)=> {
                            res.send(JSON.stringify(advertisiment))
                        }, (e)=> {
                            res.status(400).send(e)
                        })
                    })
                })
            } else {
                res.status(400).send('invalid company')
            }
        })
    })
}