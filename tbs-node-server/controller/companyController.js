
var {Company} = require('../models/company')


// get Companys
exports.getCompanyList = function(req, res) {
    Company.find().then((companyList) => {
        res.status(200).send(JSON.stringify(companyList))
    }, (e) => {
        res.status(400).send(e)
    })
}

//get Company's names list

exports.getCompanyNameList = function(req, res) {
    Company.find().then((companyList)=> {
        let companyNameList= []
        companyList.forEach((company)=> {
            companyNameList.push(company.name)
        })
        let response = {
            companyNames: companyNameList
        }
        res.status(200).send(JSON.stringify(response))
    }, (e)=> {
        res.status(400).send(e)
    })
}

// create Companys

exports.createCompany = function (req, res) {

    let company = new Company({
        name: req.body.name,
        cnpj: req.body.cnpj,
        members: [],
        advertisements: []
    })

    company.save().then(()=> {
        res.sendStatus(201)
    },(e) => {
        res.status(400).send({e})
    })
}


// update Companys
exports.updateCompany = function (req, res) {

    Company.findOneAndUpdate({name: req.params.name}, req.body).then((company) => {
        res.status(200).send(req.body)
    }, (e) =>{
        res.sendStatus(400)
    })

}

// DELETE Companys

exports.deleteCompany = function (req, res) {
    Company.findOneAndRemove({name: req.params.name}).then((company)=> {
        res.sendStatus(200)
    }, (e) => {
        res.sendStatus(400)
    })
}