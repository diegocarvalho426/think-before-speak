
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/tbs', { useNewUrlParser: true });

module.exports = {mongoose}
