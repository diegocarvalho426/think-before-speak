mongoose = require('mongoose');
validator = require('validator');


//schema for company model

var CompanySchema = mongoose.Schema({
    name: {
        type: String,
        unique: true,
    },

    cnpj: {
        type: Number,
        unique: true
    },

    members: [{
        email: String,
    }],

    advertisements: [{
        text: {
            type: String,
        },
        date: String,
        createdBy: String,

    }]
})

var Company = mongoose.model('Company',CompanySchema)

module.exports = {Company}