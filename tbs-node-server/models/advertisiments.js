mongoose = require('mongoose');
Company = require('../models/company')
validator = require('validator');

var AdvertisimentsSchema = new mongoose.Schema({
     text: {
         type: String
     },

     date: {
         type: String,
     },

     user: {
       email: String
     },

     companyName: {
         type: String,
         required: true
     },

     results:{
         type: Number,
     }
})


var Advertisiments = mongoose.model('Advertisiments', AdvertisimentsSchema)

module.exports = {Advertisiments}

