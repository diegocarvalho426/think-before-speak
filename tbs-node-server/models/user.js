
mongoose = require('mongoose');
validator = require('validator');
jwt = require('jsonwebtoken')
bcrypt = require('bcryptjs');

var UserSchema = new mongoose.Schema({
        name: {
            required: true,
            type: String,
            minLength: 6,
        },

        companyName: {
            type: String,
            required: true
        },

        email: {
            required: true,
            type: String,
            unique: true,
            validate: {
                validator: validator.isEmail,
                message:'{value} is not valid'
            }
        },

        password: {
            required: true,
            type: String,
            minLength: 6,
        },

        advertisements: [{
            text: {
                type: String,
            },

            date: {
                type: String,
            },

            results:{
                type: String,
            }

        }],

        tokens: [{
            access: {
                type: String,
                required: true,
            },
            token: {
                type: String
            }
        }]
    })

UserSchema.methods.generateAuthToken = function () {
    var user = this
    var access = 'auth'
    var token = jwt.sign({_id: user._id.toHexString(), access},'abc123').toString()
    user.tokens = user.tokens.concat([{access, token}])
    console.log(user)

    return user.save().then(() => {
        return token
    })

}


UserSchema.statics.findByToken = function(token) {

  var user = this
  var decoded

  try {
    decoded = jwt.verify(token,'abc123')
  } catch (e) {
      return Promise.reject()
  }

  return User.findOne({
    '_id': decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth',
  })
}

UserSchema.statics.findByCredentials = function(email, password) {
  var User = this
  return User.findOne({email}).then((user) => {
    if(!user){
      return Promise.reject()
    }

    return new Promise((resolve, reject) => {
      bcrypt.compare(password,user.password,(err, res) => {
        if(res){

              resolve(user)
        } else {
          reject()
        }
      })
    })
  })
}

UserSchema.pre('save', function (next) {
  var user = this
  if( user.isModified('password')){
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(user.password, salt, (err, hash) => {
        user.password = hash
        next()
      })
    })
  } else {
    next()
  }
})


var User = mongoose.model('User',UserSchema)

module.exports = {User}
