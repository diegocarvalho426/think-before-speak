var express = require('express');
var router = express.Router();
var userController = require('../controller/userController');
var companyController = require('../controller/companyController')
var {authenticate} = require('../middleware/authenticate');

/* GET users listing. */
router.get('/users', userController.getUsersList)

/*GET user account. */
router.get('/users/me',authenticate , userController.getMe)

/*GET user tweets that are already evaluated. */
router.get('/me/advertisements', authenticate, userController.getAdvertisements)

//create new user
router.post('/users'  , userController.createUser)

//login users
router.post('/users/login', userController.userLogin)

//logout users
router.delete('/users/logout', authenticate, userController.userLogout)

//update users
router.put('/users/me', authenticate, userController.userUpdate)

//remove users
router.delete('/users/me', authenticate, userController.userDelete)

//recieve Advertising
router.post('/me/advertisements',authenticate, userController.evaluateAdvertising)

//get all companie names
router.get('/company/names', companyController.getCompanyNameList)

//delete advertisiment for user
router.delete('/me/advertisements',authenticate, userController.deleteAdvertisement)

//get Sharing Area of company

router.get('/me/shared', authenticate, userController.getSharingAre)


module.exports = router;
