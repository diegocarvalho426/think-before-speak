var express = require('express')
var router = express.Router()
var companyController = require('../controller/companyController')
var advertisimentsController = require('../controller/advertisimentsController')


//get Company's list

router.get('/company', companyController.getCompanyList)

//create Company
router.post('/company', companyController.createCompany)

//remove Company
router.delete('/company/:name', companyController.deleteCompany)

//update Company
router.put('/company/:name', companyController.updateCompany)

//get Advertisiments List
router.get('/advertisiments', advertisimentsController.getAdvertisimentsList)

module.exports = router