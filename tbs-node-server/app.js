var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var app = express();
var express = require('express')
var cors = require('cors')
var app = express()

app.use(cors())

var {mongoose} = require('./config/database')
var indexRouter = require('./routes/index')
var userRouter = require('./routes/userRoutes')
var adminRouter = require('./routes/adminRoutes')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/api/v1', userRouter)
app.use('/api/v1/admin', adminRouter)

app.listen(3000, () =>{
  console.log('app started')
})

module.exports = app
