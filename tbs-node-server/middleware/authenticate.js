var {User}= require('../models/user');

var authenticate = (req, res, next) =>{
    var token = req.header('x-auth')
    User.findByToken(token).then((user) => {
        if (!user) {
            return Promise.reject()
        }

        req.user = user
        req.token = token
        try {
            next()
        } catch(e){
            console.log(e)
        }
    }).catch((e) =>{
        res.sendStatus(401)
    })
}

module.exports = {authenticate}
